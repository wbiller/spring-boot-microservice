package io.bitbucket.wbiller.archetype.springboot.core.users;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

  @Mock
  private UserRepository repository;

  private PasswordEncoder encoder;

  private UserService service;

  @Before
  public void setUp() {
    doAnswer(AdditionalAnswers.returnsFirstArg()).when(repository).save(any(User.class));
    encoder = spy(new BCryptPasswordEncoder(12));
    service = new UserServiceImpl(repository, encoder);
  }

  @Test
  public void create() throws UsernameTakenException {
    doReturn(false).when(repository).existsByUsername(anyString());

    User created = service.create("test", "test");

    assertThat(created).isNotNull();
    assertThat(created.getUsername()).isEqualTo("test");

    verify(repository).save(any(User.class));
    verify(encoder).encode(anyString());
  }

  @Test(expected = UsernameTakenException.class)
  public void create_UsernameTakenException() throws UsernameTakenException {
    doReturn(true).when(repository).existsByUsername(anyString());
    service.create("test", "test");
  }
}