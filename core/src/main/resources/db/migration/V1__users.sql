create table users (
  id bigserial not null primary key,
  username varchar(100) not null unique,
  password varchar(60) not null
);