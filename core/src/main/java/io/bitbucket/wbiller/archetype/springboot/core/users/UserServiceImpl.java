package io.bitbucket.wbiller.archetype.springboot.core.users;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;

@Service
class UserServiceImpl implements UserService {

  private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

  private final UserRepository repository;
  private final PasswordEncoder encoder;

  UserServiceImpl(UserRepository repository, PasswordEncoder encoder) {
    this.repository = repository;
    this.encoder = encoder;
  }

  @Override
  @Transactional
  public User create(String username, String password) throws UsernameTakenException {
    Assert.hasText(username, "username must not be empty");
    Assert.hasText(password, "password must not be empty");

    if(!repository.existsByUsername(username)) {
      LOG.info("Creating new user with username '{}'", username);
      return repository.save(new User(username, encoder.encode(password)));
    }

    throw new UsernameTakenException(username);
  }
}
