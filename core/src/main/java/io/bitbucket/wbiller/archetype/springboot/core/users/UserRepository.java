package io.bitbucket.wbiller.archetype.springboot.core.users;

import org.springframework.data.repository.Repository;

import java.util.Optional;

public interface UserRepository extends Repository<User, Long> {

  Optional<User> findByUsername(String username);

  boolean existsByUsername(String username);

  <E extends User> E save(E entity);

  void deleteAll();
}
