package io.bitbucket.wbiller.archetype.springboot.core.users;

public class UsernameTakenException extends Exception {

  public UsernameTakenException(String username) {
    super(String.format("Username %s is taken", username));
  }
}
