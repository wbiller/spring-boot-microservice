package io.bitbucket.wbiller.archetype.springboot.core.users;

import org.springframework.core.style.ToStringCreator;
import org.springframework.data.domain.Persistable;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "users")
@Access(AccessType.FIELD)
public class User implements Persistable<Long> {


  /* members **********************************************************************************************************/

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(nullable = false, unique = true, length = 100, updatable = false)
  private String username;

  @Column(nullable = false, length = 60)
  private String password;


  /* constructors *****************************************************************************************************/

  protected User() { } // required by JPA

  public User(String username, String password) {
    Assert.hasText(username, "username must not be empty");
    Assert.hasText(password, "password must not be empty");

    this.username = username.toLowerCase();
    this.password = password;
  }


  /* properties *******************************************************************************************************/

  @Override
  public Long getId() {
    return id;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  public String getUsername() {
    return username;
  }


  /* methods **********************************************************************************************************/

  @Override
  public boolean equals(Object obj) {
    if(null == obj) return false;
    if(this == obj) return true;
    if(getClass() != obj.getClass()) return true;

    User that = (User) obj;

    return Objects.equals(username, that.username);
  }

  @Override
  public int hashCode() {
    return Objects.hash(username);
  }

  @Override
  public String toString() {
    return new ToStringCreator(this).append("username", username).toString();
  }

  public UserDetails toSpringSecurityUser() {
    return new org.springframework.security.core.userdetails.User(
        username, password, AuthorityUtils.createAuthorityList("ROLE_USER"));
  }
}
