package io.bitbucket.wbiller.archetype.springboot.core.users;

public interface UserService {

  User create(String username, String password) throws UsernameTakenException;
}
