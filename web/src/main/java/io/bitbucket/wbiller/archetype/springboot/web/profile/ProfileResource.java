package io.bitbucket.wbiller.archetype.springboot.web.profile;

import io.bitbucket.wbiller.archetype.springboot.core.users.User;
import org.springframework.hateoas.ResourceSupport;

class ProfileResource extends ResourceSupport {

  private User user;

  ProfileResource(User user) {
    this.user = user;
  }

  public String getUsername() {
    return user.getUsername();
  }
}
