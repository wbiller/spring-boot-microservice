package io.bitbucket.wbiller.archetype.springboot.web.profile;

import io.bitbucket.wbiller.archetype.springboot.core.users.User;
import io.bitbucket.wbiller.archetype.springboot.core.users.UserRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Optional;

@RestController
@RequestMapping("/me")
class ProfileController {

  private final UserRepository repository;
  private final ProfileResourceAssembler assembler;

  ProfileController(UserRepository repository) {
    this.repository = repository;
    this.assembler = new ProfileResourceAssembler();
  }

  @GetMapping
  ResponseEntity<?> me(Principal principal) {
    Optional<User> maybeUser = repository.findByUsername(principal.getName());
    if(!maybeUser.isPresent()) {
      return ResponseEntity.notFound().build();
    }

    return ResponseEntity.ok(assembler.toResource(maybeUser.get()));
  }
}
