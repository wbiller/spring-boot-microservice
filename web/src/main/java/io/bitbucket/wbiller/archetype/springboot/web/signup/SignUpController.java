package io.bitbucket.wbiller.archetype.springboot.web.signup;

import io.bitbucket.wbiller.archetype.springboot.core.users.User;
import io.bitbucket.wbiller.archetype.springboot.core.users.UserService;
import io.bitbucket.wbiller.archetype.springboot.core.users.UsernameTakenException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponents;

@RestController
@RequestMapping("/users")
class SignUpController {

  private final UserService service;

  SignUpController(UserService service) {
    this.service = service;
  }

  @PostMapping
  ResponseEntity<?> signUp(@RequestBody SignUpCommand command) {

    try {
      command.perform(service);
      UriComponents uri = ServletUriComponentsBuilder.fromCurrentContextPath().pathSegment("me").build();
      return ResponseEntity.created(uri.toUri()).build();

    } catch(UsernameTakenException ex) {
      return ResponseEntity.status(HttpStatus.CONFLICT).build();
    }
  }
}
