package io.bitbucket.wbiller.archetype.springboot.web.profile;

import io.bitbucket.wbiller.archetype.springboot.core.users.User;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

class ProfileResourceAssembler extends ResourceAssemblerSupport<User, ProfileResource> {

  public ProfileResourceAssembler() {
    super(ProfileController.class, ProfileResource.class);
  }

  @Override
  public ProfileResource toResource(User user) {
    ProfileResource resource = new ProfileResource(user);
    resource.add(linkTo(ProfileController.class).withSelfRel());

    return resource;
  }
}
