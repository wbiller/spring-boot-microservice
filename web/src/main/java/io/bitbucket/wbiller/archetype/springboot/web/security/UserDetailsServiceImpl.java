package io.bitbucket.wbiller.archetype.springboot.web.security;

import io.bitbucket.wbiller.archetype.springboot.core.users.User;
import io.bitbucket.wbiller.archetype.springboot.core.users.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Optional;

@Service
class UserDetailsServiceImpl implements UserDetailsService {

  private final UserRepository repository;

  UserDetailsServiceImpl(UserRepository repository) {
    this.repository = repository;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Assert.hasText(username, "username must not be empty");

    Optional<User> user = repository.findByUsername(username.toLowerCase());
    if(!user.isPresent()) {
      throw new UsernameNotFoundException(String.format("Username '%s' does not exist", username));
    }

    return user.get().toSpringSecurityUser();
  }
}
