package io.bitbucket.wbiller.archetype.springboot.web.signup;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.bitbucket.wbiller.archetype.springboot.core.users.User;
import io.bitbucket.wbiller.archetype.springboot.core.users.UserService;
import io.bitbucket.wbiller.archetype.springboot.core.users.UsernameTakenException;
import org.springframework.util.Assert;

class SignUpCommand {

  private final String username;
  private final String password;

  @JsonCreator
  SignUpCommand(@JsonProperty(value = "username", required = true) String username,
                @JsonProperty(value = "password", required = true) String password) {
    Assert.hasText(username, "username must not be empty");
    Assert.hasText(password, "password must not be empty");

    this.username = username;
    this.password = password;
  }

  User perform(UserService service) throws UsernameTakenException {
    return service.create(username, password);
  }
}
