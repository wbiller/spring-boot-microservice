package io.bitbucket.wbiller.archetype.springboot.web.signup;

import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import io.bitbucket.wbiller.archetype.springboot.core.users.UserService;
import io.bitbucket.wbiller.archetype.springboot.web.AbstractControllerIT;
import io.restassured.http.ContentType;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.Matchers.endsWith;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;

public class SignUpControllerIT extends AbstractControllerIT {

  @Test
  @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED,
                    value = "SignUpControllerIT.ok.expected.xml")
  public void ok() throws JSONException {
    JSONObject command = new JSONObject()
        .put("username", "test")
        .put("password", "test");

    given().
        contentType(ContentType.JSON).
        body(command.toString()).
        postProcessors(csrf()).
    when().
        post("/users").
    then().
        statusCode(HttpStatus.CREATED.value()).
        header(HttpHeaders.LOCATION, endsWith("/me"));
  }
}