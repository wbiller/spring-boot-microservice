package io.bitbucket.wbiller.archetype.springboot.web.security;

import io.bitbucket.wbiller.archetype.springboot.core.users.User;
import io.bitbucket.wbiller.archetype.springboot.core.users.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class UserDetailsServiceImplTest {

  @Mock
  private UserRepository repository;

  private UserDetailsService service;

  @Before
  public void setUp() {
    service = new UserDetailsServiceImpl(repository);
  }

  @Test
  public void loadUserByUsername() {
    doReturn(Optional.of(new User("test", "test")))
        .when(repository).findByUsername(anyString());

    UserDetails details = service.loadUserByUsername("test");

    assertThat(details).isNotNull();
    assertThat(details.getUsername()).isEqualTo("test");
    assertThat(details.getPassword()).isEqualTo("test");
    assertThat(details.getAuthorities()).hasSize(1);
  }

  @Test(expected = UsernameNotFoundException.class)
  public void loadUserByUsername_UsernameNotFoundException() {
    doReturn(Optional.empty()).when(repository).findByUsername(anyString());
    service.loadUserByUsername("test");
  }
}