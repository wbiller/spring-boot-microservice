package io.bitbucket.wbiller.archetype.springboot.web;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import io.bitbucket.wbiller.archetype.springboot.Application;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(classes = Application.class,
                webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestExecutionListeners(mergeMode = TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS,
                        listeners = { DbUnitTestExecutionListener.class,
                                      TransactionDbUnitTestExecutionListener.class})
@Sql("/delete-all.sql")
public abstract class AbstractControllerIT {

  @Autowired
  protected MockMvc mvc;

  @Before
  public void restAssured() {
    RestAssuredMockMvc.mockMvc(mvc);
  }
}
