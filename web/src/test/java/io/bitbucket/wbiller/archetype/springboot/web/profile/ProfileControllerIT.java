package io.bitbucket.wbiller.archetype.springboot.web.profile;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import io.bitbucket.wbiller.archetype.springboot.web.AbstractControllerIT;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;

public class ProfileControllerIT extends AbstractControllerIT {

  @Test
  @DatabaseSetup("ProfileControllerIT.ok.xml")
  public void ok() {
    given().
        auth().with(httpBasic("test", "test")).
    when().
        get("/me").
    then().
        statusCode(HttpStatus.OK.value()).
        body("username", equalTo("test"));
  }

}